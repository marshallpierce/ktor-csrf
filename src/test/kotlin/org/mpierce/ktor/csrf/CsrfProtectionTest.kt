package org.mpierce.ktor.csrf

import io.ktor.http.Headers
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.application.install
import io.ktor.server.response.respond
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import io.ktor.server.testing.TestApplicationRequest
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class CsrfProtectionTest {

    @Test
    internal fun originMatchesKnownHostWithNoHeadersRejected() {
        simpleValidatorTest(OriginMatchesKnownHost("http", "csrf.test"), HttpStatusCode.BadRequest, false) {
        }
    }

    @Test
    internal fun originMatchesKnownHostWithInvalidOriginHeaderRejected() {
        simpleValidatorTest(OriginMatchesKnownHost("http", "csrf.test"), HttpStatusCode.BadRequest, false) {
            addHeader("Origin", "http://nope.wrong")
        }
    }

    @Test
    internal fun originMatchesKnownHostWithValidOriginHeaderAccepted() {
        simpleValidatorTest(OriginMatchesKnownHost("http", "csrf.test"), HttpStatusCode.NoContent, true) {
            addHeader("Origin", "http://csrf.test")
        }
    }

    @Test
    internal fun originMatchesKnownHostWithPortWithValidOriginHeaderWithPortAccepted() {
        simpleValidatorTest(
            OriginMatchesKnownHost("http", "csrf.test", 1234),
            HttpStatusCode.NoContent,
            true
        ) {
            addHeader("Origin", "http://csrf.test:1234")
        }
    }

    @Test
    internal fun originMatchesKnownHostWithPortWithValidOriginHeaderWithoutPortRejected() {
        simpleValidatorTest(
            OriginMatchesKnownHost("http", "csrf.test", 1234),
            HttpStatusCode.BadRequest,
            false
        ) {
            addHeader("Origin", "http://csrf.test")
        }
    }

    @Test
    internal fun originMatchesHostHeaderWithNoHeadersRejected() {
        simpleValidatorTest(OriginMatchesHostHeader(), HttpStatusCode.BadRequest, false) {
        }
    }

    @Test
    internal fun originMatchesHostHeaderWithNoOriginHeaderRejected() {
        simpleValidatorTest(OriginMatchesHostHeader(), HttpStatusCode.BadRequest, false) {
            addHeader("Host", "csrf.test")
        }
    }

    @Test
    internal fun originMatchesHostHeaderWithNoHostHeaderRejected() {
        simpleValidatorTest(OriginMatchesHostHeader(), HttpStatusCode.BadRequest, false) {
            addHeader("Origin", "http://csrf.test")
        }
    }

    @Test
    internal fun originMatchesHostHeaderWithInvalidOriginHeaderRejected() {
        simpleValidatorTest(OriginMatchesHostHeader(), HttpStatusCode.BadRequest, false) {
            addHeader("Host", "csrf.test")
            addHeader("Origin", "http://nope.wrong")
        }
    }

    @Test
    internal fun originMatchesHostHeaderWithValidOriginHeaderOk() {
        simpleValidatorTest(OriginMatchesHostHeader(), HttpStatusCode.NoContent, true) {
            addHeader("Host", "csrf.test")
            addHeader("Origin", "http://csrf.test")
        }
    }

    @Test
    internal fun customHeaderPresentWithHeaderOk() {
        simpleValidatorTest(HeaderPresent("X-Foo"), HttpStatusCode.NoContent, true) {
            addHeader("Host", "csrf.test")
            addHeader("X-Foo", "whatever")
        }
    }

    @Test
    internal fun customHeaderPresentWithoutValidCustomHeaderRejected() {
        simpleValidatorTest(HeaderPresent("X-Foo"), HttpStatusCode.BadRequest, false) {
            addHeader("Host", "csrf.test")
            addHeader("X-Bar", "whatever")
        }
    }

    @Test
    internal fun usesCustomResponseForFailure() {
        val protectedEndpointCalled = AtomicBoolean(false)
        withTestApplication({
            install(CsrfProtection) {
                validate(AlwaysRejectValidator())
                handleValidationFailure {
                    call.respond(HttpStatusCode.InternalServerError)
                }
            }
            configureTestEndpoints(protectedEndpointCalled)
        }) {
            with(
                handleRequest(HttpMethod.Get, "/endpoint") {
                }
            ) {
                assertEquals(HttpStatusCode.InternalServerError, response.status())
                assertFalse(protectedEndpointCalled.get())
            }
        }
    }

    @Test
    internal fun rejectsIfAnyValidatorFails() {
        val protectedEndpointCalled = AtomicBoolean(false)
        withTestApplication({
            install(CsrfProtection) {
                validate(AlwaysApproveValidator())

                validate(AlwaysRejectValidator())
            }
            configureTestEndpoints(protectedEndpointCalled)
        }) {
            with(
                handleRequest(HttpMethod.Get, "/endpoint") {
                }
            ) {
                assertEquals(HttpStatusCode.BadRequest, response.status())
                assertFalse(protectedEndpointCalled.get())
            }
        }
    }

    @Test
    internal fun acceptsIfAllValidatorPass() {
        val protectedEndpointCalled = AtomicBoolean(false)

        withTestApplication({
            install(CsrfProtection) {
                repeat(2) {
                    validate(AlwaysApproveValidator())
                }
            }
            configureTestEndpoints(protectedEndpointCalled)
        }) {
            with(
                handleRequest(HttpMethod.Get, "/endpoint") {
                }
            ) {
                assertEquals(HttpStatusCode.NoContent, response.status())
                assertTrue(protectedEndpointCalled.get())
            }
        }
    }

    @Test
    fun noCsrfProtectionEndpointAccessible() {
        simpleValidatorTest(OriginMatchesHostHeader(), HttpStatusCode.Created, false, "/noCsrfProtection", {})
    }

    @Test
    internal fun noCsrfRouteInsideCsrfRouteDoesNotApplyProtection() {
        withTestApplication({
            install(CsrfProtection) {
                validate(AlwaysRejectValidator())
            }
            routing {
                csrfProtection {
                    noCsrfProtection {
                        get("/endpoint") {
                            call.respond(HttpStatusCode.Created)
                        }
                    }
                }
            }
        }) {
            with(handleRequest(HttpMethod.Get, "/endpoint")) {
                assertEquals(HttpStatusCode.Created, response.status())
            }
        }
    }

    @Test
    internal fun csrfRouteInsideNoCsrfRouteDoesApplyProtection() {
        withTestApplication({
            install(CsrfProtection) {
                validate(AlwaysRejectValidator())
            }
            routing {
                noCsrfProtection {
                    csrfProtection {
                        get("/endpoint") {
                            call.respond(HttpStatusCode.Created)
                        }
                    }
                }
            }
        }) {
            with(handleRequest(HttpMethod.Get, "/endpoint")) {
                assertEquals(HttpStatusCode.BadRequest, response.status())
            }
        }
    }

    @Test
    internal fun defaultDoesNotValidate() {
        withTestApplication({
            install(CsrfProtection) {
                validate(AlwaysRejectValidator())
            }
            routing {
                get("/endpoint") {
                    call.respond(HttpStatusCode.Created)
                }
            }
        }) {
            with(handleRequest(HttpMethod.Get, "/endpoint")) {
                assertEquals(HttpStatusCode.Created, response.status())
            }
        }
    }

    @Test
    internal fun defaultWithApplyToAllRoutesDoesValidate() {
        withTestApplication({
            install(CsrfProtection) {
                applyToAllRoutes()
                validate(AlwaysRejectValidator())
            }
            routing {
                get("/endpoint") {
                    call.respond(HttpStatusCode.Created)
                }
            }
        }) {
            with(handleRequest(HttpMethod.Get, "/endpoint")) {
                assertEquals(HttpStatusCode.BadRequest, response.status())
            }
        }
    }

    @Test
    internal fun csrfRouteWithApplyToAllRoutesDoesValidate() {
        withTestApplication({
            install(CsrfProtection) {
                applyToAllRoutes()
                validate(AlwaysRejectValidator())
            }
            routing {
                csrfProtection {
                    get("/endpoint") {
                        call.respond(HttpStatusCode.Created)
                    }
                }
            }
        }) {
            with(handleRequest(HttpMethod.Get, "/endpoint")) {
                assertEquals(HttpStatusCode.BadRequest, response.status())
            }
        }
    }

    @Test
    internal fun noCsrfRouteWithApplyToAllRoutesDoesNotValidate() {
        withTestApplication({
            install(CsrfProtection) {
                applyToAllRoutes()
                validate(AlwaysRejectValidator())
            }
            routing {
                noCsrfProtection {
                    get("/endpoint") {
                        call.respond(HttpStatusCode.Created)
                    }
                }
            }
        }) {
            with(handleRequest(HttpMethod.Get, "/endpoint")) {
                assertEquals(HttpStatusCode.Created, response.status())
            }
        }
    }

    private fun simpleValidatorTest(
        validator: RequestValidator,
        statusCode: HttpStatusCode,
        protectedEndpointCalled: Boolean,
        path: String = "/endpoint",
        requestConfig: TestApplicationRequest.() -> Unit
    ) {
        val mutableEndpointCalled = AtomicBoolean(false)

        withTestApplication({
            install(CsrfProtection) {
                validate(validator)
            }
            configureTestEndpoints(mutableEndpointCalled)
        }) {
            with(handleRequest(HttpMethod.Get, path, requestConfig)) {
                assertEquals(statusCode, response.status())
                assertEquals(protectedEndpointCalled, mutableEndpointCalled.get())
            }
        }
    }

    private fun Application.configureTestEndpoints(protectedEndpointCalled: AtomicBoolean) {
        routing {
            csrfProtection {
                get("/endpoint") {
                    protectedEndpointCalled.set(true)
                    call.respond(HttpStatusCode.NoContent)
                }
            }
            get("/noCsrfProtection") {
                call.respond(HttpStatusCode.Created)
            }
        }
    }
}

class AlwaysApproveValidator : RequestValidator {
    override fun validate(headers: Headers): Boolean = true
}

class AlwaysRejectValidator : RequestValidator {
    override fun validate(headers: Headers): Boolean = false
}
