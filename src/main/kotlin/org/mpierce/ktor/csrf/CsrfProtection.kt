package org.mpierce.ktor.csrf

import io.ktor.http.Headers
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.ApplicationCallPipeline
import io.ktor.server.application.BaseApplicationPlugin
import io.ktor.server.application.call
import io.ktor.server.application.plugin
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.RouteSelector
import io.ktor.server.routing.RouteSelectorEvaluation
import io.ktor.server.routing.RoutingResolveContext
import io.ktor.server.routing.application
import io.ktor.server.routing.routing
import io.ktor.util.AttributeKey
import io.ktor.util.pipeline.PipelineInterceptor
import io.ktor.util.pipeline.PipelinePhase
import java.net.MalformedURLException
import java.net.URL

/**
 * Ktor feature for CSRF protection.
 */
class CsrfProtection(config: Configuration) {
    private val validators = config.validators.toList()
    private val applyToAllRoutes = config.applyToAllRoutes
    private val handleValidationFailure = config.failureResponse

    class Configuration {
        internal val validators = mutableListOf<RequestValidator>()

        internal var applyToAllRoutes = false
        internal var failureResponse: PipelineInterceptor<Unit, ApplicationCall> = {
            call.response.headers.append("X-CSRF-Rejected", "1")
            call.respond(HttpStatusCode.BadRequest)
        }

        /**
         * Protect all routes by default. If this is not called, only routes wrapped with [csrfProtection] will be
         * protected.
         *
         * To selectively remove certain routes from CSRF protection, see [noCsrfProtection].
         */
        fun applyToAllRoutes() {
            applyToAllRoutes = true
        }

        /**
         * Validate the request headers with the provided validator.
         *
         * If called multiple times, all provided validators must pass to approve a request.
         */
        fun validate(rv: RequestValidator) {
            validators.add(rv)
        }

        /**
         * Supply a custom response mechanism on failure
         */
        fun handleValidationFailure(handler: PipelineInterceptor<Unit, ApplicationCall>) {
            failureResponse = handler
        }
    }

    internal fun interceptPipelineInRoute(route: Route, protected: Boolean) {
        // seems that this phase isn't present in the per-route pipeline yet since the pipelines haven't been merged,
        // so we add it to avoid errors when inserting below.
        route.addPhase(PhaseAfterRoutes)

        route.insertPhaseBefore(PhaseAfterRoutes, PhaseInRoute)
        route.intercept(PhaseInRoute) {
            call.attributes.put(ApplyCsrfProtection, protected)
        }
    }

    companion object Feature : BaseApplicationPlugin<Application, Configuration, CsrfProtection> {
        override val key = AttributeKey<CsrfProtection>("CsrfProtection")

        val ApplyCsrfProtection = AttributeKey<Boolean>("ApplyCsrfProtection")
        private val PhaseInRoute = PipelinePhase("CsrfProtectionInRoute")
        private val PhaseAfterRoutes = PipelinePhase("CsrfProtectionAfterRoutes")

        override fun install(pipeline: Application, configure: Configuration.() -> Unit): CsrfProtection {
            val csrf = CsrfProtection(Configuration().apply(configure))
            val routing = pipeline.routing { }
            routing.insertPhaseAfter(ApplicationCallPipeline.Plugins, PhaseAfterRoutes)
            routing.intercept(PhaseAfterRoutes) {
                val applyProtection = call.attributes.getOrNull(ApplyCsrfProtection)
                if ((csrf.applyToAllRoutes && applyProtection != false) || applyProtection == true) {
                    if (csrf.validators.any { !it.validate(call.request.headers) }) {
                        csrf.handleValidationFailure(this, Unit)
                        finish()
                    }
                }
            }
            return csrf
        }
    }
}

/**
 * Apply CSRF protection (as configured for the feature) to any child routes.
 *
 * The [CsrfProtection] feature must already be installed to use this.
 *
 * Overrides any enclosing [noCsrfProtection].
 */
fun Route.csrfProtection(build: Route.() -> Unit): Route {
    val protectedRoute = createChild(CsrfRouteSelector())

    application.plugin(CsrfProtection).interceptPipelineInRoute(protectedRoute, true)
    protectedRoute.build()
    return protectedRoute
}

/**
 * Don't apply CSRF protection to any child routes, even if [org.mpierce.ktor.csrf.CsrfProtection.Configuration.applyToAllRoutes] is configured.
 *
 * Overrides any enclosing [csrfProtection].
 */
fun Route.noCsrfProtection(build: Route.() -> Unit): Route {
    val unprotectedRoute = createChild(CsrfRouteSelector())

    application.plugin(CsrfProtection).interceptPipelineInRoute(unprotectedRoute, protected = false)
    unprotectedRoute.build()
    return unprotectedRoute
}

internal class CsrfRouteSelector : RouteSelector() {
    override fun evaluate(context: RoutingResolveContext, segmentIndex: Int): RouteSelectorEvaluation {
        return RouteSelectorEvaluation.Constant
    }
}

interface RequestValidator {
    fun validate(headers: Headers): Boolean
}

/**
 * Validates that `Origin` matches a specific host (presumably, the host that this service is deployed at).
 *
 * If port is unspecified here, the urls in the headers will match only if they don't have a port specified either.
 */
class OriginMatchesKnownHost(scheme: String, host: String, port: Int? = null) : RequestValidator {
    private val host = HostTuple(scheme, host, port)

    override fun validate(headers: Headers): Boolean {
        val origin = headerUrl(headers, "Origin") ?: return false

        return host.matches(origin)
    }
}

/**
 * Validates that the `Origin` header matches the host specified in the Host header.
 */
class OriginMatchesHostHeader : RequestValidator {
    override fun validate(headers: Headers): Boolean {
        val host = headers["Host"] ?: false
        val origin = headerUrl(headers, "Origin") ?: return false

        return host == origin.host
    }
}

private fun headerUrl(headers: Headers, name: String): URL? {
    return (headers[name] ?: return null).let {
        try {
            URL(it)
        } catch (e: MalformedURLException) {
            return null
        }
    }
}

/**
 * Require that the given header be present in each request.
 */
class HeaderPresent(private val name: String) : RequestValidator {
    override fun validate(headers: Headers): Boolean {
        return headers.contains(name)
    }
}

internal class HostTuple(
    private val scheme: String,
    private val host: String,
    private val port: Int? = null
) {
    init {
        if (port != null && port < 0) {
            throw IllegalArgumentException("Port must be nonnegative or null")
        }
    }

    fun matches(url: URL): Boolean {
        // if port is unspecified in url, that's exposed as -1, so look for that if port is not set
        val expectedPort = port ?: -1
        return scheme == url.protocol && host == url.host && expectedPort == url.port
    }
}
