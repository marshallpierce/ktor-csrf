import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URI
import java.time.Duration


plugins {
    id("org.jetbrains.kotlin.jvm") version "1.6.21"
    id("org.jetbrains.dokka") version "1.6.21"
    `maven-publish`
    signing
    id("io.github.gradle-nexus.publish-plugin") version "1.1.0"
    id("net.researchgate.release") version "2.8.1"
    id("com.github.ben-manes.versions") version "0.42.0"
    id("org.jmailen.kotlinter") version "3.10.0"
}

val deps by extra {
    mapOf(
        "ktor" to "2.0.2",
        "junit" to "5.7.2"
    )
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    api("io.ktor:ktor-server-core:${deps["ktor"]}")

    testImplementation("io.ktor:ktor-server-test-host:${deps["ktor"]}")

    testRuntimeOnly("ch.qos.logback:logback-classic:1.2.3")

    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
    testImplementation(kotlin("test-junit5"))
}

java {
    targetCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    test {
        useJUnitPlatform()
    }

    register<Jar>("docJar") {
        from(project.tasks["dokkaHtml"])
        archiveClassifier.set("javadoc")
    }

    afterReleaseBuild {
        dependsOn(provider { project.tasks.named("publishToSonatype") })
    }
}

group = "org.mpierce.ktor.csrf"

publishing {
    publications {
        register<MavenPublication>("sonatype") {
            from(components["java"])
            artifact(tasks["docJar"])
            // sonatype required pom elements
            pom {
                name.set("${project.group}:${project.name}")
                description.set(name)
                url.set("https://bitbucket.org/marshallpierce/ktor-csrf")
                licenses {
                    license {
                        name.set("Copyfree Open Innovation License 0.5")
                        url.set("https://copyfree.org/content/standard/licenses/coil/license.txt")
                    }
                }
                developers {
                    developer {
                        id.set("marshallpierce")
                        name.set("Marshall Pierce")
                        email.set("575695+marshallpierce@users.noreply.github.com")
                    }
                }
                scm {
                    connection.set("scm:git:https://bitbucket.org/marshallpierce/ktor-csrf")
                    developerConnection.set("scm:git:ssh://git@bitbucket.org:marshallpierce/ktor-csrf.git")
                    url.set("https://bitbucket.org/marshallpierce/ktor-csrf")
                }
            }
        }
    }

    // A safe throw-away place to publish to:
    // ./gradlew publishSonatypePublicationToLocalDebugRepository -Pversion=foo
    repositories {
        maven {
            name = "localDebug"
            url = URI.create("file:///${project.buildDir}/repos/localDebug")
        }
    }
}

// don't barf for devs without signing set up
if (project.hasProperty("signing.keyId")) {
    signing {
        sign(project.extensions.getByType<PublishingExtension>().publications["sonatype"])
    }
}

nexusPublishing {
    repositories {
        sonatype {
            // sonatypeUsername and sonatypePassword properties are used automatically
            stagingProfileId.set("ab8c5618978d18") // org.mpierce
        }
    }
    // these are not strictly required. The default timeouts are set to 1 minute. But Sonatype can be really slow.
    // If you get the error "java.net.SocketTimeoutException: timeout", these lines will help.
    connectTimeout.set(Duration.ofMinutes(3))
    clientTimeout.set(Duration.ofMinutes(3))
}
